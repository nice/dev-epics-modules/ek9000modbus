
ek9000modbus
======
European Spallation Source ERIC Site-specific EPICS module : ek9000modbus

Additonal information:

* [Documentation](https://confluence.esss.lu.se/display/IS/Integration+by+ICS)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/e3-recipes/ek9000modbus-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)